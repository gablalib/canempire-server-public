#FROM adoptopenjdk/openjdk11-openj9:jdk-11.0.1.13-alpine-slim
#ENV PROJECT_ID=ambient-antenna-263905
#COPY build/libs/canempire-server-*-all.jar canempire-server.jar
#EXPOSE 8080
#CMD java -Dcom.sun.management.jmxremote -noverify ${JAVA_OPTS} -jar canempire-server.jar

FROM gradle:jdk8
VOLUME /tmp
RUN mkdir /work
COPY . /work
WORKDIR /work
RUN gradle build
RUN mv /work/build/libs/canempire-server-*-all.jar /work/app.jar
EXPOSE 8080
ENTRYPOINT ["java","-jar","/work/app.jar"]