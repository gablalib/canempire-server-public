package com.gablalib.canempire.controllers

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get

@Controller("/heartbeat")
class HeartbeatController {

    @Get
    fun heartbeat(): Boolean = true
}