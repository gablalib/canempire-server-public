package com.gablalib.canempire.controllers.review

import com.gablalib.canempire.controllers.review.requests.GetNewReviewsRequest
import com.gablalib.canempire.controllers.review.requests.GetReviewsRequest
import com.gablalib.canempire.controllers.review.requests.PublishReviewRequest
import com.gablalib.canempire.controllers.review.responses.GetReviewsResponse
import com.gablalib.canempire.models.review.Review
import com.gablalib.canempire.services.review.ReviewFetcher
import com.gablalib.canempire.services.review.ReviewPublisher
import io.micronaut.http.annotation.Body
import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Post

@Controller("/reviews")
class ReviewController {

    @Post("/publish")
    fun publish(@Body publishReviewRequest: PublishReviewRequest)
        = ReviewPublisher.publish(publishReviewRequest)

    @Post("/latest")
    fun getLatest(@Body getReviewsRequest: GetReviewsRequest): GetReviewsResponse
        = ReviewFetcher.getLatest(getReviewsRequest)

    @Post("/best")
    fun getBest(@Body getReviewsRequest: GetReviewsRequest): GetReviewsResponse
        = ReviewFetcher.getBest(getReviewsRequest)

    @Post("/new")
    fun getNew(@Body getNewReviewsRequest: GetNewReviewsRequest): GetReviewsResponse
        = ReviewFetcher.getNew(getNewReviewsRequest)
}
