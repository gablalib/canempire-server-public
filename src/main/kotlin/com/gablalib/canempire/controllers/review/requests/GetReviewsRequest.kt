package com.gablalib.canempire.controllers.review.requests

data class GetReviewsRequest(val amount: Int,
                             val offset: Int,
                             val lastUpdated: Long)