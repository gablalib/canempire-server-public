package com.gablalib.canempire.controllers.review.responses

import com.gablalib.canempire.models.review.Review

data class GetReviewsResponse(val reviews: List<Review>,
                              val lastUpdated: Long)