package com.gablalib.canempire.controllers.review.requests

data class PublishReviewRequest(val reviewId: String,
                                val userId: String,
                                val timestamp: Long,
                                val description: String = "",
                                val product: String,
                                val stars: Int,
                                val image: String = "")