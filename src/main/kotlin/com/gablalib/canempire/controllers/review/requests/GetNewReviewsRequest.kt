package com.gablalib.canempire.controllers.review.requests

data class GetNewReviewsRequest(val since: Long)