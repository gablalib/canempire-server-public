package com.gablalib.canempire.controllers.product.requests

import com.gablalib.canempire.models.product.Product

data class PatchProductRequest(val product: Product)