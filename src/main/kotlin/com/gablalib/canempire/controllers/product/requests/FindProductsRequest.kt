package com.gablalib.canempire.controllers.product.requests

import com.gablalib.canempire.models.product.ProductsFilter

data class FindProductsRequest(val filter: ProductsFilter)