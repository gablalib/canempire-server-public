package com.gablalib.canempire.controllers.product

import com.gablalib.canempire.controllers.product.requests.FindProductsRequest
import com.gablalib.canempire.controllers.product.requests.PatchProductRequest
import com.gablalib.canempire.models.product.Product
import com.gablalib.canempire.services.product.ProductFetcher
import com.gablalib.canempire.services.product.ProductPatcher
import io.micronaut.http.annotation.*

@Controller("/products")
class ProductController {

    @Get
    fun getAll(): List<Product>
            = ProductFetcher.fetchAll()

    @Put("/{productId}")
    fun publish(@Body data: PatchProductRequest,
                @PathVariable productId: String)
            = ProductPatcher.patch(data, productId)

    @Post("/find")
    fun find(@Body data: FindProductsRequest): List<Product>
            = ProductFetcher.fetch(data)
}
