package com.gablalib.canempire.controllers.exceptionhandlers

import com.gablalib.canempire.models.exceptions.BadRequestException
import io.micronaut.http.HttpRequest
import io.micronaut.http.HttpResponse
import io.micronaut.http.server.exceptions.ExceptionHandler
import javax.inject.Singleton

@Singleton
object BadRequestExceptionHandler: ExceptionHandler<BadRequestException, HttpResponse<Unit>> {

    override fun handle(request: HttpRequest<*>?, exception: BadRequestException?): HttpResponse<Unit> {
        return HttpResponse.badRequest()
    }
}