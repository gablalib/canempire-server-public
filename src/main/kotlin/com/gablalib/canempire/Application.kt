package com.gablalib.canempire

import com.google.firebase.FirebaseApp
import com.google.firebase.FirebaseOptions
import io.micronaut.runtime.Micronaut

object Application {
    private val GOOGLE_PROJECT_ID = System.getenv("PROJECT_ID")

    @JvmStatic
    fun main(args: Array<String>) {
        initFirestore()
        Micronaut.build()
                .packages("com.gablalib.canempire")
                .mainClass(Application.javaClass)
                .args("-v")
                .start()
    }

    fun initFirestore() {
        val options = FirebaseOptions.Builder()
                .setProjectId(GOOGLE_PROJECT_ID)
                .build()

        FirebaseApp.initializeApp(options)
    }
}