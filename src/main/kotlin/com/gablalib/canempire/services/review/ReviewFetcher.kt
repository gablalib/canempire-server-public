package com.gablalib.canempire.services.review

import com.gablalib.canempire.collections.datasource.Firestore
import com.gablalib.canempire.controllers.review.requests.GetNewReviewsRequest
import com.gablalib.canempire.controllers.review.requests.GetReviewsRequest
import com.gablalib.canempire.controllers.review.responses.GetReviewsResponse
import com.gablalib.canempire.models.review.Review

object ReviewFetcher {

    private val firestore = Firestore

    fun getLatest(request: GetReviewsRequest): GetReviewsResponse {
        val snapshots = firestore.fetchLatest(request.offset, request.amount, request.lastUpdated).get()
        return GetReviewsResponse(snapshots.toObjects(Review::class.java), System.currentTimeMillis())
    }

    fun getBest(request: GetReviewsRequest): GetReviewsResponse {
        val snapshots = firestore.fetchBest(request.offset, request.amount, request.lastUpdated).get()
        return GetReviewsResponse(snapshots.toObjects(Review::class.java), System.currentTimeMillis())
    }

    fun getNew(request: GetNewReviewsRequest): GetReviewsResponse {
        val snapshots = firestore.fetchNew(request.since).get()
        return GetReviewsResponse(snapshots.toObjects(Review::class.java), System.currentTimeMillis())
    }
}