package com.gablalib.canempire.services.review

import com.gablalib.canempire.controllers.review.requests.PublishReviewRequest
import com.gablalib.canempire.collections.datasource.Firestore
import com.google.firebase.cloud.FirestoreClient

object ReviewPublisher {

    private val firestore = Firestore

    fun publish(request: PublishReviewRequest) {
        firestore.addDocumentToCollection(
                hashMapOf(
                        "id" to request.reviewId,
                        "userId" to request.userId,
                        "product" to request.product,
                        "description" to request.description,
                        "stars" to request.stars,
                        "timestamp" to System.currentTimeMillis(),
                        "image" to request.image
                ), "reviews-dev"
        )
    }
}