package com.gablalib.canempire.services.product

import com.gablalib.canempire.collections.ProductCollection
import com.gablalib.canempire.controllers.product.requests.FindProductsRequest
import com.gablalib.canempire.models.product.Product

object ProductFetcher {
    fun fetch(request: FindProductsRequest): List<Product>
            = ProductCollection.find(request.filter)

    fun fetchAll(): List<Product>
            = ProductCollection.findAll()
}