package com.gablalib.canempire.services.product

import com.gablalib.canempire.collections.ProductCollection
import com.gablalib.canempire.controllers.product.requests.PatchProductRequest
import com.gablalib.canempire.models.exceptions.BadRequestException

object ProductPatcher {

    fun patch(request: PatchProductRequest, productId: String) {
        if (productId == request.product.id)
            ProductCollection.patch(request.product)
        else
            throw BadRequestException()
    }
}