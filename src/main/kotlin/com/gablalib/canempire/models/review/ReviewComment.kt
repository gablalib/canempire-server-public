package com.gablalib.canempire.models.review

data class ReviewComment(val reviewId: String,
                         val commentId: String,
                         val userId: String,
                         val description: String,
                         val likes: ArrayList<String> = ArrayList(),
                         val likesCount: Int = 0,
                         val dislikes: ArrayList<String> = ArrayList(),
                         val dislikeCount: Int = 0)