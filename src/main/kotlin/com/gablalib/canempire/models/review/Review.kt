package com.gablalib.canempire.models.review

import com.gablalib.canempire.collections.Identifiable

data class Review(override val id: String = "",
                  val userId: String = "",
                  val product: String = "",
                  val description: String = "",
                  val stars: Int = 0,
                  val timestamp: Long = 0,
                  val image: String? = "",
                  val likes: ArrayList<String> = ArrayList(),
                  val likesCount: Int = 0,
                  val dislikes: ArrayList<String> = ArrayList(),
                  val dislikesCount: Int = 0): Identifiable