package com.gablalib.canempire.models.product

data class ProductFormat(val id: String = "",
                         val quantity: String = "",
                         val price: String = "",
                         val unit: String = "")