package com.gablalib.canempire.models.product

import com.gablalib.canempire.collections.Identifiable

data class Product(override val id: String = "",
                   val name: String = "",
                   val brand: String = "",
                   val producer: String = "",
                   val description: String = "",
                   val type: String = "",
                   val category: String = "",
                   val dominance: String = "",
                   val intensity: String = "",
                   val minThc: Int = 0,
                   val maxThc: Int = 0,
                   val thcUnit: String = "",
                   val minCbd: Int = 0,
                   val maxCbd: Int = 0,
                   val cbdUnit: String = "",
                   val formats: List<ProductFormat> = emptyList()): Identifiable