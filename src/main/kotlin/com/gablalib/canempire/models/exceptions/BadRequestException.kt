package com.gablalib.canempire.models.exceptions

class BadRequestException(message: String? = ""): RuntimeException(message)