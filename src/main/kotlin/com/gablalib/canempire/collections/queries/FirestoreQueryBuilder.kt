package com.gablalib.canempire.collections.queries

import com.gablalib.canempire.models.product.ProductsFilter
import com.google.cloud.firestore.Query

object FirestoreQueryBuilder {

    fun <F> build(query: Query, filter: F): Query {
        return when (filter) {
            is ProductsFilter -> buildProductQuery(query, filter)
            else -> query
        }
    }

    private fun buildProductQuery(query: Query, filter: ProductsFilter): Query {
        return query.whereGreaterThanOrEqualTo("name", filter.name ?: "")
    }
}