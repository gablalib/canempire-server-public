package com.gablalib.canempire.collections

interface Collection<T> {
    fun get(id: String): T?
    fun patch(item: T): Boolean
    fun <F> find(filter: F?): List<T>
    fun findAll(): List<T>
}