package com.gablalib.canempire.collections

import com.gablalib.canempire.collections.datasource.DataSource
import com.gablalib.canempire.collections.datasource.Firestore
import com.gablalib.canempire.models.product.Product

object ProductCollection: Collection<Product> {

    private const val PRODUCT_COLLECTION_NAME = "products"

    private val dataSource: DataSource = Firestore

    override fun get(id: String): Product? {
        return dataSource.findById(id).get()
                .toObjects(Product::class.java)[0]
    }

    override fun patch(item: Product): Boolean {
        return this.dataSource.patchDocument(item, PRODUCT_COLLECTION_NAME)
    }

    override fun <F> find(filter: F?): List<Product> {
        return dataSource.findByFilter(filter, PRODUCT_COLLECTION_NAME)
                .get().toObjects(Product::class.java)
    }

    override fun findAll(): List<Product> {
        return dataSource.findAllInCollection(PRODUCT_COLLECTION_NAME)
                .get().toObjects(Product::class.java)
    }
}