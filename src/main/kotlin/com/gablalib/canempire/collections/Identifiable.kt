package com.gablalib.canempire.collections

interface Identifiable {
    val id: String
}