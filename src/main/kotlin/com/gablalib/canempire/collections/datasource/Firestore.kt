package com.gablalib.canempire.collections.datasource

import com.gablalib.canempire.collections.Identifiable
import com.gablalib.canempire.collections.queries.FirestoreQueryBuilder
import com.gablalib.canempire.models.product.ProductsFilter
import com.google.api.core.ApiFuture
import com.google.cloud.firestore.DocumentReference
import com.google.cloud.firestore.Query
import com.google.cloud.firestore.QuerySnapshot
import com.google.cloud.firestore.SetOptions
import com.google.firebase.cloud.FirestoreClient

object Firestore: DataSource {
    private const val DEFAULT_LIMIT = 10

    private val firestore = FirestoreClient.getFirestore()

    override fun addDocumentToCollection(document: Map<String, Any>, collection: String): Boolean {
        val docRef: DocumentReference = firestore.collection(collection).document(document.get("id").toString())
        return docRef.set(document).isDone
    }

    override fun patchDocument(document: Identifiable, collection: String): Boolean {
        val docRef: DocumentReference = firestore.collection(collection).document(document.id)
        return docRef.set(document, SetOptions.merge()).isDone
    }

    override fun findAllInCollection(collection: String): ApiFuture<QuerySnapshot> {
        return firestore.collection(collection).get()
    }

    override fun <F> findByFilter(filter: F?, collection: String): ApiFuture<QuerySnapshot> {
        val reference = firestore.collection(collection)
        val query = FirestoreQueryBuilder.build(reference, filter)

        return query.get()
    }

    override fun findById(id: String): ApiFuture<QuerySnapshot> {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }


    fun fetchLatest(offset: Int = 0, amount: Int = DEFAULT_LIMIT, lastUpdated: Long): ApiFuture<QuerySnapshot>
            = firestore.collection("reviews-dev")
            .orderBy("timestamp", Query.Direction.DESCENDING)
            .whereLessThanOrEqualTo("timestamp", lastUpdated)
            .offset(offset)
            .limit(amount)
            .get()

    fun fetchBest(offset: Int = 0, amount: Int = DEFAULT_LIMIT, lastUpdated: Long): ApiFuture<QuerySnapshot>
            = firestore.collection("reviews-dev")
            .orderBy("stars", Query.Direction.DESCENDING)
            .whereLessThanOrEqualTo("timestamp", lastUpdated)
            .offset(offset)
            .limit(amount)
            .get()

    fun fetchNew(since: Long): ApiFuture<QuerySnapshot>
            = firestore.collection("reviews-dev")
            .orderBy("timestamp", Query.Direction.DESCENDING)
            .whereGreaterThan("timestamp", since)
            .get()

    fun patchProduct(document: Map<String, Any>, collection: String)
            = firestore.collection(collection)
            .document(document.get("id").toString())
            .set(document)

    fun fetchProducts(filter: ProductsFilter?): ApiFuture<QuerySnapshot>
            = firestore.collection("products")
            .whereGreaterThanOrEqualTo("name", filter?.name ?: "")
            .get()
}