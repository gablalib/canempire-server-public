package com.gablalib.canempire.collections.datasource

import com.gablalib.canempire.collections.Identifiable
import com.google.api.core.ApiFuture
import com.google.cloud.firestore.QuerySnapshot

interface DataSource {
    fun addDocumentToCollection(document: Map<String, Any>, collection: String): Boolean
    fun patchDocument(document: Identifiable, collection: String): Boolean
    fun findById(id: String): ApiFuture<QuerySnapshot>
    fun <F> findByFilter(filter: F?, collection: String): ApiFuture<QuerySnapshot>
    fun findAllInCollection(collection: String): ApiFuture<QuerySnapshot>
}