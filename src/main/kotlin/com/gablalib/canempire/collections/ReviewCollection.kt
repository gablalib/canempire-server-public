package com.gablalib.canempire.collections

import com.gablalib.canempire.collections.datasource.DataSource
import com.gablalib.canempire.collections.datasource.Firestore
import com.gablalib.canempire.models.review.Review
import com.google.firebase.cloud.FirestoreClient

object ReviewCollection: Collection<Review> {

    private const val REVIEW_COLLECTION_NAME = "reviews-dev"

    private val dataSource: DataSource = Firestore

    override fun get(id: String): Review? {
        return dataSource.findById(id).get()
                .toObjects(Review::class.java)[0]
    }

    override fun patch(item: Review): Boolean {
        return this.dataSource.patchDocument(item, REVIEW_COLLECTION_NAME)
    }

    override fun <F> find(filter: F?): List<Review> {
        return dataSource.findByFilter(filter, REVIEW_COLLECTION_NAME)
                .get().toObjects(Review::class.java)
    }

    override fun findAll(): List<Review> {
        return dataSource.findAllInCollection(REVIEW_COLLECTION_NAME)
                .get().toObjects(Review::class.java)
    }
}