#!/bin/bash

gradle assemble
docker build -t canempire-server:latest .
docker tag canempire-server:latest gablalib/canempire-server:latest
docker push gablalib/canempire-server:latest
kubectl set image deployment/canempire-deployment server=gablalib/canempire-server:latest
#kubectl delete deployment canempire-deployment
#kubectl apply -f k8s/deployment.yaml